﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview_Test
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] Origin = File.ReadAllLines(@"..\..\Files\MOCK_DATA.csv");

            Origin = Origin.Where((source, index) => index != 0).ToArray();

            string[][] NewFormat = new string[Origin.Length*4][];

            NewFormat[0] = new string[] { "Flag1", "Flag2", "Flag3", "Flag4", "Flag5", "Name", "Address", "City", "State", "Zip" };

            int i = 1;
            foreach (var line in Origin)
            {
                var values = line.Split(',');
                for(int j=5; j<9; j++)
                {
                    if (!values[j].Equals(""))
                    {
                        List<string> newLine = values.Skip(0).Take(5).ToArray().ToList();

                        for (int k=5, z=0; k < 10; k++, z+=4)
                        {
                            newLine.Add(values[j+z]);
                        }
                            
                        NewFormat[i++] = newLine.ToArray();
                    }
                }
            }
            NewFormat = NewFormat.Skip(0).Take(i).ToArray();

            string FilePath = @"..\..\Files\NewFormat.csv";
            string Seperator = ",";
            StringBuilder Builder = new StringBuilder();
            foreach(var line in NewFormat)
            {
                Builder.AppendLine(string.Join(Seperator, line));
            }
            File.WriteAllText(FilePath, Builder.ToString());

        }
    }
}
